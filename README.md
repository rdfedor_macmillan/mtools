# Introduction

This is a tool set used to help perform automated tasks for Moldraw by creating a framework to 
which the simple scripts can be set up and incorporated into.  When running the tool set, a list 
of available commands can be viewed by passing -h.

# Setup

To start using the available tools, run the following command to install the necessary tools,

```npm install```

# Development

To run the development version of the tool set without building the code, run

```./dev-run-mtools.sh```

# Building

To build the tool set so it doesn't have to be transpiled on every execution, run

```npm run build```

Then it can be ran by running,

```./run-tools.sh```