#!/bin/sh
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
npx $DIR/../node_modules/.bin/babel-node $DIR/../index.js ${@}