import CoreCommand from './lib/core/command'

const coreCommand = new CoreCommand();

coreCommand.loadCommands();

coreCommand.parse(...process.argv);