import path from "path";
import { writeFile, readFile, mkdir, exists } from "../core/file";
import CommandInterface from "./command-interface";

/**
 * Formats the moldraw payload by sorting the item list so it's consistent between
 * comparisons
 */
class CommandFormatMoldrawPayload extends CommandInterface {
  getCommandName() {
    return "explode-iac-response";
  }

  getCommandAlias() {
    return "eir";
  }

  getCommandDescription() {
    return "Converts an IAC response into a folder/file based structure for easy searching";
  }

  getCommandOptions() {
    return [
      ["-f, --file [path]", "The path to the json file to process"],
      ["-o, --output [path]", "The path to output the json results to"],
    ];
  }

  async getCommandAction({ file, output }) {
    if (!file) {
      throw new Error(
        `File is a required parameters to run this command. (File : ${file})`
      );
    }

    if (!output) {
      output = `${process.cwd()}`;
    }

    let filepath = path.resolve(process.cwd(), file);

    if (file.substr(0, 1) === "/") {
      filepath = path.resolve(file);
    }

    try {
      const payload = JSON.parse(await readFile(filepath));

      if (!(await exists(output))) {
        await mkdir(output);
      }

      await Promise.all(
        payload.map((student) => {
          const { id: assignmentId, course_id, user_id, items } = student;
          const studentFileName = `Course-${course_id}-Assignment-${assignmentId}-Student-${user_id}`;
          const summarizedStudentObj = { ...student };
          delete summarizedStudentObj.items;

          return Promise.all(
            [
              writeFile(
                `${output}/${studentFileName}.txt`,
                JSON.stringify(summarizedStudentObj, null, 2)
              ),
            ].concat(
              items.map((question, index) => {
                const { id } = question;

                writeFile(
                  `${output}/${studentFileName}-Question-${index + 1}-${id}.txt`,
                  JSON.stringify(question, null, 2)
                );
              })
            )
          );
        })
      );
    } catch (err) {
      console.error(err);
    }

    console.log("Done");
  }
}

export default CommandFormatMoldrawPayload;
