/**
 * Command Interface class for use when creating new commands
 */
class CommandInterface {
    /**
     * @param {import("commander").Command} program Instance of the command program to add new commands to
     */
    constructor(program) {
        this.getProgram = _ => program;

        const commandName = this.getCommandName();
        const commandAlias = this.getCommandAlias();
        const commandDescription = this.getCommandDescription();
        const commandOptions = this.getCommandOptions();

        const command = program.command(commandName);

        if (commandAlias) {
            command.alias(commandAlias);
        }

        if (commandDescription) {
            command.description(commandDescription);
        }

        if (commandOptions) {
            commandOptions.forEach(option => command.option(...option));
        }

        command.action(this.getCommandAction);

        return command;
    }

    /**
     * The long name of the command
     * @returns {false|string}
     */
    getCommandName() {
        return false;
    }

    /**
     * The short name of the command
     * @returns {false|string}
     */
    getCommandAlias() {
        return false;
    }

    /**
     * The description of the command
     * @returns {false|string}
     */
    getCommandDescription() {
        return false;
    }

    /**
     * The options associated with the commands
     * @returns {false|string}
     */
    getCommandOptions() {
        return false;
    }

    /**
     * The action executed when the command is ran
     * @returns {false|string}
     */
    getCommandAction(options) {
        return false;
    }
}

export default CommandInterface;