import path from 'path';
import CommandInterface from './command-interface';
import { writeFile, readFile } from '../core/file';

/**
 * Formats the moldraw payload by sorting the item list so it's consistent between
 * comparisons
 */
class CommandFormatMoldrawPayload extends CommandInterface {
    getCommandName() {
        return 'format-moldraw-payload';
    }

    getCommandAlias() {
        return 'fmp';
    }

    getCommandDescription() {
        return 'Normalizes and formats moldraw payload';
    }

    getCommandOptions() {
        return [
            ['-f, --file [path]', 'The path to the json file to process'],
            ['-o, --output [path]', 'The path to output the json results to'],
        ];
    }

    async getCommandAction({ file, output }) {
        if (!file) {
            throw new Error(`File and output are required parameters to run this command. (File : ${file})`);
        }

        let filepath = path.resolve(process.cwd(), file);

        if (file.substr(0,1) === '/') {
            filepath = path.resolve(file);
        }

        const payload = JSON.parse(await readFile(filepath));

        payload.root.children.items = payload.root.children.items.sort((a,b) => a.id <= b.id);

        const data = JSON.stringify(payload, null, "\t");

        if (output) {
            await writeFile(`${process.cwd()}/${output}`, data);
            console.log(`Successfully wrote results to ${path.resolve(`${process.cwd()}/${output}`)}`);
        } else {
            console.log(data);
        }
    }
}

export default CommandFormatMoldrawPayload;