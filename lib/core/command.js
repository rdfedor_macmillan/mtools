import Commander from 'commander'
import * as AvailableCommands from '../commands'

/**
 * Abstract interface that pulls the various command line tools together
 * @class CoreCommand
 */
class CoreCommand {
    constructor() {
        const command = new Commander.Command();

        /**
         * @returns {import('commander').Command}
         */
        this.getCommand = _ => command;

        command.description('Moldraw CLI Tools');
    }

    loadCommands() {
        const commandProgram = this.getCommand();

        Object.keys(AvailableCommands).forEach(command => {
            commandProgram.addCommand(new AvailableCommands[command](commandProgram));
        });
    }

    /**
     * Parses the command line arguments against the interface library
     * @param  {...any} args list of command line arguments to process
     */
    parse(...args) {
        this.getCommand().parse(args);
    }
}

export default CoreCommand;