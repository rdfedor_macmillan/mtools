const fs = require('fs');
const util = require('util');

export const readFile = util.promisify(fs.readFile);

export const writeFile = util.promisify(fs.writeFile);

export const mkdir = util.promisify(fs.mkdir);

export const exists = util.promisify(fs.exists);

export default {
    readFile,
    writeFile,
    mkdir,
    exists
}